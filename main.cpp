/*
Copyright 2015-2018 Endre Karlson <endre.karlson@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// For printf used below
#include <stdio.h>
#include <gflags/gflags.h>
#include <mqtt/async_client.h>

// PJON library
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

// RPI serial interface
#include <iostream>
#include <fstream>
#include <string>
#include <thread>

#include <wiringPi.h>
#include <wiringSerial.h>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include <json.hpp>

std::shared_ptr<spdlog::logger> logger;

DEFINE_string(mqtt_host, "tcp://localhost:1883", "MQTT host to use");
DEFINE_string(mqtt_client_id, "pjon-gateway", "MQTT Client id to use");

#ifndef RPI
#define RPI true
#endif

#define TS_RESPONSE_TIME_OUT 0
#define PJON_INCLUDE_ASYNC_ACK true
#define PJON_INCLUDE_TS true

int RECEIVE_LATENCY = 6000;

#include "third_party/PJON/src/PJON.h"

PJON<ThroughSerial> bus(1);
mqtt::async_client *mqtt_client;

// using namespace protocol;
int last_attempt;

int init_serial()
{
  logger->debug("Opening serial...");
  int s = serialOpen("/dev/ttyUSB0", 9600);
  if (s < 0)
  {
    logger->critical("Error opening serial port {}", "/dev/ttyUSB0");
  }

  if (wiringPiSetup() == -1)
  {
    logger->critical("WiringPi setup fail");
  }

  logger->debug("Serial setup complete.");
  return s;
}

void subscribe(mqtt::async_client *client)
{
  client->subscribe("relay", 1)->wait();
  logger->debug("Subscribed to topics");
}

bool try_reconnect(mqtt::async_client *cli)
{
  constexpr int N_ATTEMPT = 30;

  for (int i = 0; i < N_ATTEMPT && !cli->is_connected(); ++i)
  {
    try
    {
      cli->reconnect();
      subscribe(cli);
      return true;
    }
    catch (const mqtt::exception &e)
    {
    }
  }

  return false;
}

bool ensure_connected(mqtt::async_client *cli)
{
  if (!cli->is_connected())
  {
    int now = time(NULL);
    int diff = now - last_attempt;

    //printf("%i", diff);
    if (diff >= 5)
    {
      last_attempt = time(NULL);
      logger->info("Reconnecting.");

      try
      {
        cli->reconnect();
        subscribe(cli);
      }
      catch (const mqtt::exception &)
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  last_attempt = 0;
  return true;
}

// int run_forever(PJON<ThroughSerial> *bus, mqtt::async_client *cli) {
int run_forever(mqtt::async_client *cli)
{
  /*
   * Uses a synchronous architecture currently where data sent by the server is
   * processed first and then data from devices
  */

  //mqtt::const_message_ptr *msg = std::make_shared<mqtt::const_message_ptr>();
  mqtt::const_message_ptr received_msg = std::make_shared<mqtt::message>();

  logger->info("Starting loop");
  while (true)
  {
    try
    {
      bool is_connected = ensure_connected(cli);

      if (is_connected)
      {
        //logger->debug("Pre consume");
        bool consumed = cli->try_consume_message(&received_msg);
        //logger->debug("Post consume");

        if (consumed) {
          logger->debug("Consumed message");
          std::string payload = received_msg->to_string();
          int length = payload.length();

          bus.send(45, payload.c_str(), length);

          logger->debug("Forwarded msg {} ", payload);
        }
      }

      bus.receive(TS_TIME_IN + RECEIVE_LATENCY);
      bus.update();
    }
    catch (const mqtt::exception &exc)
    {
      logger->error(exc.what());
    }
  }

  return 1;
}

void receiver_function(uint8_t *payload, uint16_t length,
                       const PJON_Packet_Info &packet_info)
{
  logger->debug("Received from PJON");
  std::string raw = reinterpret_cast<char *>(payload);
  std::string data = raw.substr(0, length);

  auto pubmsg = mqtt::make_message("gw/outbound", data);
  mqtt_client->publish(pubmsg);
}

void error_handler(uint8_t code, uint16_t data, void *custom_pointer)
{
  logger->error("Error code {} data {}", code, data);
}

int main(int argc, char **argv)
{
  gflags::SetUsageMessage("PJON mqtt gateway");
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  gflags::SetVersionString("0.1.0");

  logger = spdlog::stdout_color_mt("console");
  logger->set_level(spdlog::level::debug);

  int s = init_serial();
  bus.strategy.set_serial(s);

  bus.strategy.set_enable_RS485_pin(29);
  bus.strategy.set_baud_rate(9600);

  bus.set_synchronous_acknowledge(false);
  bus.set_asynchronous_acknowledge(true);

  bus.set_receiver(receiver_function);

  bus.begin();
  bus.set_error(error_handler);
  mqtt::async_client client(FLAGS_mqtt_host, FLAGS_mqtt_client_id);
  mqtt_client = &client;

  mqtt::connect_options connOpts;
  connOpts.set_keep_alive_interval(10);
  connOpts.set_clean_session(true);

  // Try the initial connect but dont die if it doesnt work.
  try
  {
    client.connect(connOpts)->wait();
    subscribe(&client);
    client.start_consuming();
  }
  catch (const mqtt::exception &exc)
  {
    logger->error(exc.what());
  }

  run_forever(&client);
  return 0;
}
