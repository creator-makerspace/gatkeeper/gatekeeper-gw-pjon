
#include <iostream>
#include <fstream>

// For printf used below
#include <stdio.h>
// PJON library
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
// RPI serial interface
#include <wiringPi.h>
#include <wiringSerial.h>

#ifndef RPI
#define RPI true
#endif

#define PJON_INCLUDE_TS true

#define ASYNC true

#if ASYNC == true
#define TS_RESPONSE_TIME_OUT 0
#define PJON_INCLUDE_ASYNC_ACK true
#else
#define TS_RESPONSE_TIME_OUT 35000
#endif

#include "PJON.h"

uint32_t RECEIVE_LATENCY = 10000;

// Bus id definition
uint8_t bus_id_A[] = {0, 0, 0, 1};
uint8_t bus_id_B[] = {0, 0, 0, 2};;

void receiver_function(uint8_t *payload, uint16_t length,
                       const PJON_Packet_Info &packet_info)
{
  std::string raw = reinterpret_cast<char *>(payload);
  std::string data = raw.substr(0, length);
  std::cout << data << std::flush;
}

int main()
{
  printf("PJON instantiation... \n");
  PJON<ThroughSerial> bus(1);

  printf("Opening serial... \n");
  int s = serialOpen("/dev/ttyAMA0", 9600);
  if (s < 0)
    printf("Serial open fail!");
  if (wiringPiSetup() == -1)
    printf("WiringPi setup fail");
  printf("Setting serial... \n");
  bus.strategy.set_serial(s);
  bus.strategy.set_enable_RS485_pin(29);
  bus.strategy.set_baud_rate(9600);

  #if ASYNC == true
      bus.set_synchronous_acknowledge(false);
      bus.set_asynchronous_acknowledge(true);
  #endif

  bus.set_receiver(receiver_function);
  bus.begin();

  while (true)
  {
    bus.update();
    bus.receive(RECEIVE_LATENCY);
  }

  return 0;
};
