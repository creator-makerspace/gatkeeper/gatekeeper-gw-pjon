#include <iostream>
#include <fstream>

// For printf used below
#include <stdio.h>
// PJON library
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
// RPI serial interface
#include <wiringPi.h>
#include <wiringSerial.h>

#ifndef RPI
#define RPI true
#endif

#define TS_RESPONSE_TIME_OUT 0
#define PJON_INCLUDE_TS true

#include "PJON/PJON.h"

void receiver_function(uint8_t *payload, uint16_t length,
                       const PJON_Packet_Info &packet_info)
{
  std::string raw = reinterpret_cast<char *>(payload);
  std::string data = raw.substr(0, length);
  std::cout << data << std::flush;
}

int main()
{
  printf("PJON instantiation... \n");
  PJON<ThroughSerial> bus(44);

  printf("Opening serial... \n");
  int s = serialOpen("/dev/ttyAMA0", 115200);
  if (s < 0)
    printf("Serial open fail!");
  if (wiringPiSetup() == -1)
    printf("WiringPi setup fail");
  printf("Setting serial... \n");
  bus.strategy.set_serial(s);
  bus.strategy.set_enable_RS485_pin(29);
  bus.strategy.set_baud_rate(115200);

  bus.set_receiver(receiver_function);

  printf("Opening bus... \n");
  //bus.begin();
  printf("Attempting to send a packet... \n");
  //bus.send(44, "B", 1);
  printf("Attempting to roll bus... \n");
  //bus.update();
  printf("Attempting to receive from bus... \n");
  //bus.receive();
  printf("Success! \n");

  while (true)
  {
    bus.update();
    bus.receive();
  }

  return 0;
};
