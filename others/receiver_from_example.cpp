
// For printf used below
#include <stdio.h>
// PJON library
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
// RPI serial interface
#include <wiringPi.h>
#include <wiringSerial.h>

#ifndef RPI
  #define RPI true
#endif

#define ASYNC true

#if ASYNC == true
#define TS_RESPONSE_TIME_OUT 0
#define PJON_INCLUDE_ASYNC_ACK true
#else
#define TS_RESPONSE_TIME_OUT 35000
#endif

/* Maximum accepted timeframe between transmission and synchronous
   acknowledgement. This timeframe is affected by latency and CRC computation.
   Could be necessary to higher this value if devices are separated by long
   physical distance and or if transmitting long packets. */

#define PJON_INCLUDE_TS true // Include only ThroughSerial
#include "PJON/PJON.h"

int RECEIVE_LATENCY = 6000;

// Bus id definition
uint8_t bus_id_A[] = {0, 0, 0, 1};
uint8_t bus_id_B[] = {0, 0, 0, 2};

void receiver_function(uint8_t *payload, uint16_t length, const PJON_Packet_Info &packet_info) {
  /* Make use of the payload before sending something, the buffer where payload points to is
     overwritten when a new message is dispatched */
  if(payload[0] == 'B') {
    printf("BLINK\n");
    fflush(stdout);
  }
};

int main() {
  printf("PJON instantiation... \n");
  PJON<ThroughSerial> bus(bus_id_A, 1);
  uint32_t baud_rate = 9600;
  printf("Opening serial... \n");
  int s = serialOpen("/dev/ttyAMA0", baud_rate);
  if(int(s) < 0) printf("Serial open fail!");
  if(wiringPiSetup() == -1) printf("WiringPi setup fail");
  printf("Setting serial... \n");
  bus.strategy.set_serial(s);
  bus.strategy.set_baud_rate(baud_rate);

#if ASYNC == true
  bus.set_asynchronous_acknowledge(true);
#endif

  bus.set_receiver(receiver_function);

  printf("Opening bus... \n");
  bus.begin();
  printf("Waiting for BLINK... \n");

  while (true)
  {
    bus.update();
    bus.receive(RECEIVE_LATENCY);
  }

  return 0;
};
