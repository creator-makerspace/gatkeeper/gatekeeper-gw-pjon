HOST_SYSTEM = $(shell uname | cut -f 1 -d_)
SYSTEM ?= $(HOST_SYSTEM)
CXX = clang++
CPPFLAGS += -I/usr/include -I/usr/local/include -Ithird_party/PJON/src -Ithird_party/json/src -Ithird_party/spdlog/include -pthread
CXXFLAGS += -std=c++11 -g

LDFLAGS += -L/usr/lib -L/usr/local/lib -L/usr/local/lib/mqtt \
           -Wl,--no-as-needed -Wl,--as-needed \
           -lpthread -ldl -lwiringPi -pthread -lpaho-mqttpp3 -lpaho-mqtt3a -lgflags

PROTOC := $(shell which protoc)
GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`

PROTOS_PATH = .

all: gateway

#gateway: gateway.pb.o gateway.grpc.pb.o main.o
gateway:  main.o
	$(CXX) $^ $(LDFLAGS) -o $@

#.PRECIOUS: %.grpc.pb.cc
#%.grpc.pb.cc: %.proto
#	$(PROTOC) -I $(PROTOS_PATH) --grpc_out=. --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $<

#.PRECIOUS: %.pb.cc
#%.pb.cc: %.proto
#	$(PROTOC) -I $(PROTOS_PATH) --cpp_out=. $<

clean:
	rm -f *.o *.pb.* gateway
